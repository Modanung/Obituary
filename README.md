# Hopeless species
by Frode 魔大农 Lindeijer - 2024  
in dedication to Julian Assange

## Games as the latest literary incarnation
The printing press was a great leap forward in communicating and preserving ideas, which led to a period in human history known as the renaissance, characterized by a multitude of advances in science and technology. Curiosity,  laziness, natural threats and visions of a better world have always been the main forces driving innovation, not necessity nor financial gain. The latter two lead to haphazard fixes and a decrease in quality, not progress.

## FLOSS for a free society
Privacy is essential for developing and expressing one’s opinion freely without repercussions. This makes free software crucial in protecting this cornerstone of a free society, since any piece of proprietary software is a potential breach of privacy.  
The most common argument given for continuing the use of proprietary operating systems is the availability of high-quality entertainment.

## Money is proxy cannibalism
Market dynamics have led us down a path of cutting corners wherever profitable. Affordable goods still rely on child slavery, despite technology creating abundance. Anything that doesn’t pay turns into a chore that almost nobody has time for. From picking up trash to lending an ear to strangers or pondering society’s greater issues, most would place themselves "above" it (in fact proving them worse people) and scorn anyone who sees the point in undertaking these thankless efforts until it can be monetized.  
Money can be seen as the only conspiracy; the conspiracy of all against all. The rest is just market dynamics.

### The belief in money gives power to the rich
The concept of money comes with a built-in feedback loop: Money controls where money goes, which over generations elevates the worst of the worst to ever more powerful positions. Its essence is purely imaginary, and its supposed value is based on the extent to which it allows one to manipulate fellow delusional humans into doing things they otherwise wouldn't. Money can therefore be best described as a persistent mass psychosis. It is the no-thing.

#### A list of misnomers
A close observation of various tax-funded industries reveals how, without changing the words we use to describe them, language has turned into Orwellian Newspeak. The map is not the territory and the territory is decaying as we (New)speak.

##### Defence
Some say that offence is the best defence. It is however the surest way to escalation. The best defence is _de_-escalation in the form of diplomacy and verbal judo. Of course, this does not require weapons and would thereby make the arms industry effectively redundant. 

##### Healthcare
The influence that the pharmaceutical industry has on the research of their own products trickles down to what doctors and nurses are taught. This has turned healthcare into an industry of sickness and death. Market dynamics have put descendents of literal snake oil salesmen in control of what you are told is good for your well-being. Meanwhile the fact that medical mishaps are a leading cause of death is obscured.

##### Economy
The word economic is synonymous with efficient. A country's GDP however represents the amount of resources being moved around, not its useful output. If all essential goals were met more efficiently, this would result in a lower GDP. Economics therefore reflect a nation's inefficiency.

## Not having a job: The greater self-sacrifice
Like a dendritic Newton's cradle, taxes pay for the bombing of children. Accepting atrocities like these, and shrugging them off as some “fact of life” in the face of which you think yourself powerless is a false notion - you are after all an active participant - that makes supposed "contributions to society" nothing but myopic sanctimony and destructive at its core. The road to hell is paved with good intentions.  
Joblessness allows one to do what is right, instead of what is demanded by the market. Of course it provides room for mindless self-indulgence, which - although less morally reprehensible than thoughtless labour - clearly is not most beneficial to society.

### Scientia potentia est
Knowledge is power, and properly informing oneself on a wide range of issues takes a lot of time. Selling your time, then, is guaranteed to have yourself be led by the proverbial nose.

## The only logical alternative
Some might say we must then return to bartering. It doesn’t take much thought however to realize that this is exactly where the unfair destructive system began. Barter _also_ relies on the logic that a person who has naught begets nothing. The only alternative therefore is learning to share.
Theft only helps to justify intrusive solutions like the placement of cameras and other forms of liberty deprivation. The same is true for vandalism and violent resistance.

### "You cannot change the world"
A common retort against idealist suggestions is that the world cannot be changed. This is the stupidest thing anyone has ever said. Obviously change is inevitable, just as is it impossible to stop time from passing. You _are_ changing the world _for the worse_, and could instead choose to change it for the better. All it takes is using your head instead of believing the spoon-fed ₿$. Grow up and take some responsibility.

### Ineffective suggestions
Over time, numerous fools have played word games and called them solutions. Let's take a closer look.

#### Decentralized finance
Bitcoin might be a nice alternative to CBDCs, but decentralizing ledgers does nothing in terms of redistributing wealth/power. It thereby maintains the status quo of empowering the greater sociopaths.

#### Anarchism
Another solution, some might say, is taking away government. Most anarchist, however, still cling to the concept of money and markets. They don't seem to realize that the problem with government is not that it exists, but rather that it is corrupted by forces they mean to uphold. Forces that - through financial means - are above the law, and can therefore be classified as anarchic. Anarchy does nothing to end plutocracy.

## Entheogenic inspiration
People are led to believe that substances used for thousands of years in shamanic traditions will drive you crazy, while a mounting body of scientific evidence points to the fact that psychedelics heighten empathy and make the brain temporarily enter a kind of dream state in which more information can be processed. Longer and more complex neural connections are formed, which in any other context would be recognized as higher intelligence. The conclusions distilled simply go over your head.  
During psychedelic experiences I have seen and felt how the tears of a African mother where caught before they could reach her thirsty infant's open mouth, syphoned off just to fill a bottle of Spa. It is only through money that such a level of moral disengagement can be achieved on a massive scale. Visions like these made me vow that I would let financial gain influence my choices as least as possible and to figure out a way to reach those who dare not tread the same path. Personally, I eventually settled on making computer games in hopes of raising human conscientiousness to a humane level. I will not stand for the current lousy keeping up of appearances while all goes to hell.
You're a heartless idiot and nature has a cure for that.

## Conclusion
It is time the proletariat got off its high horse of traditional hypocrisy, cease their profitable (false profits, mind you) procrastination... or eat me.


> I hope I made you laugh  
> I hope I made you cry  
> If this is all it does  
> then I would rather die